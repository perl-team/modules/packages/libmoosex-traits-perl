libmoosex-traits-perl (0.13-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libtest-simple-perl and
      perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 23:17:27 +0100

libmoosex-traits-perl (0.13-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:02:09 +0200

libmoosex-traits-perl (0.13-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Declare conformance with Policy 3.9.7
  * Swap the order of the alternative build-dependency on Test::Simple

 -- Damyan Ivanov <dmn@debian.org>  Mon, 22 Feb 2016 08:01:53 +0000

libmoosex-traits-perl (0.13-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: bump required debhelper version for
    Module::Build::Tiny to 9.20140227.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.
  * Add debian/upstream/metadata.
  * Import upstream version 0.13.
  * Update years of packaging copyright, and Upstream-Contact.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Explicitly (build) depend on Moose. Thanks to autopkgtest.

 -- gregor herrmann <gregoa@debian.org>  Sun, 25 Oct 2015 18:26:05 +0100

libmoosex-traits-perl (0.12-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright. Drop info about removed files.
  * Update (build) dependencies.
  * Install new CONTRIBUTING file.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Dec 2013 22:42:52 +0100

libmoosex-traits-perl (0.11-2) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Email change: Ansgar Burchardt -> ansgar@debian.org

  [ gregor herrmann ]
  * debian/control: update (build) dependencies to prepare for
    Moose/Class::MOP changes.
  * debian/copyright: update formatting
  * Set Standards-Version to 3.9.2 (no changes).
  * Add /me to Uploaders.
  * Bump debhelper compatibility level to 8.
  * debian/control: remove version from libmoosex-role-parameterized-perl,
    nothing older in the archive.

 -- gregor herrmann <gregoa@debian.org>  Thu, 21 Apr 2011 19:10:49 +0200

libmoosex-traits-perl (0.11-1) unstable; urgency=low

  * New upstream release.
  * Bump build-dep, recommends on libmoosex-role-parameterized-perl
    to >= 0.13.
  * Drop patch fix-pod-spelling.patch: applied upstream.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 14 May 2010 10:38:26 +0900

libmoosex-traits-perl (0.09-1) unstable; urgency=low

  * New upstream release
  * Use new 3.0 (quilt) source format
  * Update to new DEP5 copyright format
  * Update copyright information
  * Add a patch to fix POD spelling errors

 -- Jonathan Yu <jawnsy@cpan.org>  Wed, 07 Apr 2010 11:46:32 -0400

libmoosex-traits-perl (0.08-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Standards-Version 3.8.4 (no changes)
  * Update short description

  [ Ryan Niebur ]
  * Update jawnsy's email address
  * Update ryan52's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 15 Feb 2010 13:41:43 -0500

libmoosex-traits-perl (0.07-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + apply_traits is now deprecated (use "no warnings" to disable warning)
    + add MooseX::Traits::Util so that other modules can resolve trait names
    + add new_class_with_traits util function to create classes with traits
  * Standards-Version 3.8.3 (no changes)
  * New dependency on libnamespace-autoclean-perl
  * Added MooseX-Role-Parameterized to B-D-I for testing, and Recommends

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

 -- Jonathan Yu <frequency@cpan.org>  Fri, 21 Aug 2009 14:55:07 -0400

libmoosex-traits-perl (0.06-1) unstable; urgency=low

  * New upstream release
    + Updated M::I version
    + Bug fixes (RT#47011 and RT#46936)
  * New dependency on Moose and Class-MOP
  * Standards-Version 3.8.2

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

 -- Jonathan Yu <frequency@cpan.org>  Mon, 29 Jun 2009 07:16:07 -0400

libmoosex-traits-perl (0.05-1) unstable; urgency=low

  [ Ryan Niebur ]
  * New upstream release

  [ Jonathan Yu ]
  * New upstream release
    -> Feature change: support subclasses of classes that use MX::Traits
    -> New test added

 -- Jonathan Yu <frequency@cpan.org>  Thu, 28 May 2009 12:23:15 -0400

libmoosex-traits-perl (0.03-1) unstable; urgency=low

  * Initial Release. (Closes: #529239)

 -- Jonathan Yu <frequency@cpan.org>  Mon, 18 May 2009 22:00:43 -0400
